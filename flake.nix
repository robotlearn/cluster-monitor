{
    description = "rlan nix flake";

    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs";
    };

    outputs = { self, nixpkgs }:

        let
            pkgs = import nixpkgs {
                system = "x86_64-linux";
            };
        in {
            devShells.x86_64-linux.default = pkgs.mkShell {
                buildInputs = with pkgs; [
                    python3

                ];

                shellHook =
                let
                    packageList = with pkgs; [
                        # for PyTorch
                        stdenv.cc.cc.lib

                        # for Numpy
                        zlib
                    ];

                    libPath = nixpkgs.lib.strings.concatMapStringsSep ":" (p: "${p}/lib") packageList;
                in
                    ''
                        export LD_LIBRARY_PATH=${libPath}

                        export PYTHONPATH=$(pwd):$PYTHONPATH
                    '';
            };

        };
}

import * as utils from "./utils.js";
import { WaitingList } from "./waiting_list.js";
import { Node } from "./node.js";


export class Cluster {
    constructor() {

        // Nodes ----------------------------------------------------------
        this.nodes = {};

        this.main_container = document.createElement('div');
        this.main_container.className = 'main container';

        // Add cluster nodes 1 to 7 (they each have 2 GPUs)
        for (let node_id = 1; node_id < 8; node_id++ ) {
            this.nodes[node_id] = new Node(node_id, 2, this.main_container);
        }

        // Invisible node fills the gap at the right of 'gpu7', as longer 'gpu8' is put below
        let invisible_node = document.createElement('div');
        invisible_node.className = 'cluster container';
        invisible_node.id = 'invisible-cluster';
        this.main_container.appendChild(invisible_node);

        // Add cluster node 8 (which has 4 GPUs)
        this.nodes[8] = new Node(8, 4, this.main_container);

        let last_container = document.createElement('div');
        last_container.className = 'cluster container';
        last_container.id = 'last-cluster';
        this.main_container.appendChild(last_container);

        // Waiting list ---------------------------------------------------
        this.waiting_list = new WaitingList();
        this.main_container.appendChild(this.waiting_list.main_div);
    }

    update(dict) {
        let nodes_dict = dict['nodes'];
        for (let node_id in nodes_dict) {
            this.nodes[node_id].update(nodes_dict[node_id]);
        }

        let waiting_list_dict = dict['waiting_jobs'];
        this.waiting_list.update(waiting_list_dict);
    }

    show() {
        var root = document.getElementById('home');
        root.appendChild(this.main_container);
    }

    hide() {
    }
}

import { Field, BoolField } from "./utils.js";

export class Gpu {
    constructor(id, cluster_container) {

        this.cluster_container = cluster_container;

        this.div = document.createElement('div');
        this.div.className = 'gpu';

        // Name
        this.name_div = document.createElement('div');
        this.name_div.className = 'info-gpu container centered';
        this.name_field = new Field(`GPU ${id}`, 'model');
        this.name_div.appendChild(this.name_field.node);

        this.div.appendChild(this.name_div);

        // Job
        this.job_div = document.createElement('div');
        this.job_div.className = 'job container';

        this.state = 'FREE';

        // If the GPU is free or suspected.
        this.state_sub_div = document.createElement('div');
        this.state_sub_div.className = 'free container';
        let state_par = document.createElement('p');
        state_par.innerHTML = '<strong>FREE</strong>';
        this.state_sub_div.appendChild(state_par);
        this.state_div = document.createElement('div');
        this.state_div.className = 'job container';
        this.state_div.appendChild(this.state_sub_div);

        this.div.appendChild(this.state_div);

        // Job ID and owner
        this.job_info_div = document.createElement('div');
        this.job_info_div.className = 'job-info container';

        // Job ID
        this.job_id_field = new Field(
            'ID',
            'job_id',
            true,
            'job-id'
        );
        this.job_info_div.appendChild(this.job_id_field.node);

        // Job owner
        this.job_owner_field = new Field(
            'owner',
            'job_owner',
            true,
            'job-owner'
        );
        this.job_info_div.appendChild(this.job_owner_field.node);

        // Job type
        this.job_type_field = new Field(
            'type',
            'job_type',
            true,
            'job-type'
        );
        this.job_info_div.appendChild(this.job_type_field.node);

        this.job_div.appendChild(this.job_info_div);

        // Job name
        this.job_name_div = document.createElement('div');
        this.job_name_field = new Field(
            'name',
            'job_name',
            true,
            'job-name'
        );
        this.job_name_div.appendChild(this.job_name_field.node);
        this.job_div.appendChild(this.job_name_div);

        // Job properties
        this.properties_div = document.createElement('div');
        this.properties_div.className = 'booleans container';

        // besteffort
        this.besteffort_field = new BoolField('besteffort', 'besteffort');
        this.properties_div.appendChild(this.besteffort_field.node);

        // idempotent
        this.idempotent_field = new BoolField('idempotent', 'idempotent');
        this.properties_div.appendChild(this.idempotent_field.node);

        this.job_div.appendChild(this.properties_div);

        // Job times (submission, start, walltime...)
        this.times_div = document.createElement('div');
        this.times_div.className = 'times container';

        // Start and submit times
        this.submit_start_time_div = document.createElement('div');
        this.submit_start_time_div.className = 'two-times container';

        // Submit time
        this.submit_time_field = new Field(
            'submitted',
            '00/00 00:00:00',
            true,
            'submit-time'
        );
        this.submit_start_time_div.appendChild(this.submit_time_field.node);
        // Start time
        this.start_time_field = new Field(
            'started',
            '00/00 00:00:00',
            true,
            'start-time'
        );
        this.submit_start_time_div.appendChild(this.start_time_field.node);
        this.times_div.appendChild(this.submit_start_time_div);

        // Walltime and max time
        this.wall_max_finished_time_div = document.createElement('div');
        this.wall_max_finished_time_div.className = 'two-times container';

        // Walltime
        this.wall_time_field = new Field(
            'wall-time',
            '00:00:00',
            true,
            'wall-time'
        );
        this.wall_max_finished_time_div.appendChild(this.wall_time_field.node);

        // Max time
        this.max_finish_time_field = new Field(
            'ends before',
            '00/00 00:00:00',
            true,
            'max-finished-time'
        );
        this.wall_max_finished_time_div.appendChild(this.max_finish_time_field.node);
        this.times_div.appendChild(this.wall_max_finished_time_div);
        this.job_div.appendChild(this.times_div);

        this.cluster_container.appendChild(this.div);
    }

    update_free() {
        if (this.div.childNodes[1] === this.job_div)
            this.div.replaceChild(this.state_div, this.job_div);
        this.div.style.backgroundColor = getComputedStyle(
            document.documentElement
        ).getPropertyValue('--primary');
    }

    update_busy(dict) {
        if (this.div.childNodes[1] === this.state_div)
            this.div.replaceChild(this.job_div, this.state_div);

        this.job_id_field.set_text(dict['id']);

        this.job_owner_field.set_text(dict['owner']);
        if ('owner_color' in dict) {
            this.div.style.backgroundColor = dict['owner_color'];
        }

        let job_name = dict['name'] || '';
        this.job_name_field.set_text(job_name.slice(0, 38));
        this.job_type_field.set_text(dict['type']);
        this.idempotent_field.update(dict['idempotent']);
        this.besteffort_field.update(dict['besteffort']);
        this.submit_time_field.set_text(dict['submission_time']);
        this.start_time_field.set_text(dict['start_time']);
        this.wall_time_field.set_text(dict['walltime']);
        this.max_finish_time_field.set_text(dict['max_time']);
    }

    update_from_dict(gpu_model, dict) {
        this.name_field.set_text(gpu_model);

        if (dict == 'free')
            this.update_free();
        else {
            this.update_busy(dict);
        }
    }
}

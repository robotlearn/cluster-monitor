import { Gpu } from "./gpu.js";

export class Node {
    constructor(id, num_gpus, root_node) {
        this.root_container = root_node;

        this.id = id;
        this.container = document.createElement('div');
        this.container.id = `gpu${id}`;
        this.container.className = 'cluster container';
        this.root_container.appendChild(this.container);

        this.div_name = document.createElement('div');
        this.div_name.className = 'specs container centered';
        this.container.appendChild(this.div_name);
        this.par_name = document.createElement('p');
        this.par_name.className = 'cluster-name';
        this.par_name.innerHTML = `gpu${id}-perception`;
        this.div_name.append(this.par_name);

        this.num_gpus = num_gpus;
        this.gpus = [];
        for (let gpu_id = 0; gpu_id < this.num_gpus; gpu_id++ ) {
            this.gpus.push(new Gpu(gpu_id, this.container));
        }
    }

    update(dict) {

        this.gpu_model = dict['gpu_model'];

        for (let gpu_id = 0; gpu_id < this.num_gpus; gpu_id++ ) {
            this.gpus[gpu_id].update_from_dict(
                this.gpu_model,
                dict['gpus'][gpu_id]
            );
        }
    }
}

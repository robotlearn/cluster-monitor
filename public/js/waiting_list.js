export class WaitingList {
    constructor() {
        this.main_div = document.createElement('div');
        this.main_div.id = 'waiting-list';

        // Title
        let title = document.createElement('h');
        title.id = 'waiting-list-title';
        title.innerHTML = 'Waiting list';
        this.main_div.appendChild(title);

        let header = document.createElement('div');
        header.className = 'waiting-list-header waiting-list-row';
        let header_job_id = document.createElement('div');
        header_job_id.className = 'waiting-row-element waiting-job-id';
        header_job_id.innerHTML = 'ID';
        header.appendChild(header_job_id);

        let header_job_owner = document.createElement('div');
        header_job_owner.className = 'waiting-row-element waiting-job-owner';
        header_job_owner.innerHTML = 'owner';
        header.appendChild(header_job_owner);

        let header_submission_time = document.createElement('div');
        header_submission_time.className = 'waiting-row-element waiting-job-submission-time';
        header_submission_time.innerHTML = 'submitted';
        header.appendChild(header_submission_time);

        let header_walltime = document.createElement('div');
        header_walltime.className = 'waiting-row-element waiting-job-walltime';
        header_walltime.innerHTML = 'walltime';
        header.appendChild(header_walltime);

        let header_besteffort = document.createElement('div');
        header_besteffort.className = 'waiting-row-element waiting-job-besteffort';
        header_besteffort.innerHTML = 'besteffort';
        header.appendChild(header_besteffort);

        let header_gpu = document.createElement('div');
        header_gpu.className = 'waiting-row-element waiting-job-gpu';
        header_gpu.innerHTML = 'requested gpu';
        header.appendChild(header_gpu);

        let table = document.createElement('div');
        table.id = 'waiting-list-table';
        table.appendChild(header);
        this.table_rows = document.createElement('div');
        this.table_rows.id = 'waiting-list-table-rows';
        table.appendChild(this.table_rows);

        this.main_div.appendChild(table);
    }

    update(waiting_jobs_dict) {
        let jobs = [];

        for (var job_id in waiting_jobs_dict) {
            // Check if job already exists in the
            jobs.push(waiting_jobs_dict[job_id]);
        }

        // Sort by job ID
        jobs.sort(
            function(a, b) {
                return a['id'] - b['id'];
            }
        );

        this.table_rows.innerHTML = '';
        for (let job_dict of jobs) {

            // Create and fill row
            let row = document.createElement('div');
            row.className = 'waiting-list-row';

            let job_id = document.createElement('div');
            job_id.className = 'waiting-row-element waiting-job-id';
            job_id.innerHTML = job_dict['id'];
            row.appendChild(job_id);

            let owner = document.createElement('div');
            owner.className = 'waiting-row-element waiting-job-owner';
            owner.innerHTML = job_dict['owner'];
            row.appendChild(owner);

            if ('owner_color' in job_dict) {
                row.style.backgroundColor = job_dict['owner_color'];
            }

            let submission_time = document.createElement('div');
            submission_time.className = 'waiting-row-element waiting-job-submission-time';
            submission_time.innerHTML = job_dict['submission_time'];
            row.appendChild(submission_time);

            let walltime = document.createElement('div');
            walltime.className = 'waiting-row-element waiting-job-walltime';
            walltime.innerHTML = job_dict['walltime'];
            row.appendChild(walltime);

            let best_effort = document.createElement('div');
            best_effort.className = 'waiting-row-element waiting-job-besteffort';
            if (job_dict['besteffort']) {
                best_effort.innerHTML = '✅';
            } else {
                best_effort.innerHTML = '❌';
            }
            row.appendChild(best_effort);

            let gpu = document.createElement('div');
            gpu.className = 'waiting-row-element waiting-job-gpu';
            gpu.innerHTML = job_dict['requested_gpu'];
            row.appendChild(gpu);

            this.table_rows.appendChild(row);
        }

    }
}

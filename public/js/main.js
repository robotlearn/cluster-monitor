import { Field } from "./utils.js";
import { Cluster } from "./cluster.js";

var last_updated = new Field('Last update', '', false);
last_updated.get_node_from_id('last-updated');

var cluster = new Cluster();

// Show the default page
cluster.show();

// console.log("Connecting to socket")
var socket = io('https://ws.robotlearn.inrialpes.fr');
// var socket = io('0.0.0.0:8888');
// console.log("Succesfully connected to socket server.")

function update_timestamp() {
    let currentDate = new Date();
    let current_time = currentDate.getHours() + ':';
    current_time += String(
        currentDate.getMinutes()
    ).padStart(2, '0') + ":";
    current_time += String(
        currentDate.getSeconds()
    ).padStart(2, '0');
    last_updated.set_text(current_time);
}

socket.on(
    'update',
    function(new_dict) {
        // Timestamp
        update_timestamp();

        // Update data
        cluster.update(new_dict);
    }
);

# RobotLearn Cluster monitor

## [robotlearn.inrialpes.fr/cluster-monitor/](https://robotlearn.inrialpes.fr/cluster-monitor/)

![](screenshot.png)

## Implementation overview

The cluster monitor counts two entities:
- The **backend** server is running on an Inria machine (`robotlearn.inrialpes.fr`).
  It is performing `ssh` commands to `access2-cp` to gather the cluster state.\
  It also exposes a [Socket.IO](https://socket.io/) server to the web clients that connect to it.
  It pushes the cluster information to all of the connected clients through the _socket-io_
  connection.\
  **Code:** `backend/`
- The **frontend** is a javascript application that connects to the _socket-io_ server
  (**backend**) and updates the html page with the received data.\
  **Code:** `public/`

## Acknowledgment

- **David Emukpere:** for his numerous advice and help about web development and infrastructure.
- [**Tanguy Lepage:**](https://tanguylepage.com/) for the frontend HTML/CSS design.
- [**Anand Ballou:**](https://team.inria.fr/robotlearn/team-members/anand-ballou/) for his advice
  and his help on the cluster data fetching.

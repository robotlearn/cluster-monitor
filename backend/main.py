#!/usr/bin/env python3

from logging import Logger, getLogger
import time
from datetime import datetime

from backend._logging import init_logger
from backend import cluster

import socketio  # type: ignore
import eventlet  # type: ignore
eventlet.monkey_patch()

# Init logger
init_logger()
LOGGER: Logger = getLogger('rl_hm.web_app')

REFRESH_TIME: int = 1
DATETIME_FORMAT: str = '%m/%d/%Y-%H:%M:%S'

SOCKET_IO_PORT: int = 8888
# 16_384
MAX_MESSAGE_LENGTH: int = 2 ** 14

socket_io: socketio.Server = socketio.Server(
    async_mode='eventlet',
    cors_allowed_origins='*'
)

num_connected_clients: int = 0


def _update_loop(socket_io: socketio.Server) -> None:

    # Tracking
    init_time: datetime = datetime.now()
    step_counter: int = 1

    while True:

        LOGGER.info("%i clients are currently connected", num_connected_clients)

        if num_connected_clients > 0:

            LOGGER.info("--> updating")
            LOGGER.info("step n°%i", step_counter)
            LOGGER.info("started: %s", init_time.strftime(DATETIME_FORMAT))

            # If the update fails, just ignore it and try another time.
            try:
                payload_dict: dict = cluster.update()

                socket_io.emit(
                    event='update',
                    data=payload_dict
                )

            except Exception as exception:
                LOGGER.error("Exception:", exc_info=exception)

        # Eventually wait before updating again
        if REFRESH_TIME > 0:
            LOGGER.debug("waiting %is before updating again", REFRESH_TIME)
            time.sleep(REFRESH_TIME)

        step_counter += 1


@socket_io.on('connect')
def callback_connect(*args) -> None:
    global num_connected_clients
    num_connected_clients += 1

    LOGGER.info("A client is connected (total: %i)", num_connected_clients)

    if num_connected_clients == 1:
        LOGGER.info("This is the first client: starting to fetch cluster updates")


@socket_io.on('disconnect')
def callback_disconnect(*args) -> None:
    global num_connected_clients
    num_connected_clients = max(0, num_connected_clients - 1)

    LOGGER.info("A client has disconnected (%i remaining)", num_connected_clients)

    if num_connected_clients == 0:
        LOGGER.info("This was the last client: interrupting cluster updates fetching")


def main() -> None:

    # Run the update loop that queries cluster information
    socket_io.start_background_task(
        target=_update_loop,
        socket_io=socket_io
    )

    LOGGER.info("Starting WSGIApp")
    app: socketio.ASGIApp = socketio.WSGIApp(socket_io)
    eventlet.wsgi.server(
        eventlet.listen(
            ('0.0.0.0', SOCKET_IO_PORT)
        ),
        app
    )


if __name__ == '__main__':
    main()

import logging
from termcolor import colored  # type: ignore


class CustomFormatter(logging.Formatter):
    """
    Logging colored formatter, adapted from https://stackoverflow.com/a/56944256/3638629
    """

    def __init__(self, fmt='') -> None:
        super().__init__()

        self.fmt = fmt

        _colors: dict[int, str] = {
            logging.DEBUG: 'blue',
            logging.INFO: 'green',
            logging.WARNING: 'yellow',
            logging.ERROR: 'red',
            logging.CRITICAL: 'red'
        }

        self.formatters: dict[int, logging.Formatter] = {
            log_level: logging.Formatter(
                '%(asctime)s ' + colored('[%(levelname)s] ',
                                         color=color,
                                         attrs=['bold'] if log_level == logging.CRITICAL else [])
                + colored('%(name)s', color='magenta') + ' | %(message)s'
            )
            for log_level, color in _colors.items()
        }

    def format(self, record: logging.LogRecord) -> str:

        return self.formatters[record.levelno].format(record)


def init_logger(debug_level: int = logging.INFO) -> None:

    # Create logger
    logger: logging.Logger = logging.getLogger()
    logger.setLevel(debug_level)

    # Create console handler and set level to debug
    console_handler: logging.StreamHandler = logging.StreamHandler()
    console_handler.setLevel(debug_level)

    # Create formatter
    formatter: logging.Formatter = CustomFormatter()

    # Add formatter to console handler
    console_handler.setFormatter(formatter)

    # Add console handler to logger
    logger.addHandler(console_handler)

import json
import subprocess
from datetime import datetime, timedelta
import logging
import re
from typing import Optional, Any
from matplotlib import cm  # type: ignore
import numpy as np


TIMEOUT: int = 15
LOGGER: logging.Logger = logging.getLogger(__name__)

PRETTY_GPU_MODELS: dict[str, str] = {
    'titanx': 'Titan X',
    'titanrtx': 'Titan RTX',
    'rtx8000': 'Quadro RTX 8000'
}

TIME_FORMAT: str = '%m/%d %H:%M:%S'

USERNAMES: list[str] = [
    'aballou',
    'galepage',
    'aauterna',
    'yixu',
    'creinke',
    'xilin',
    'lairale',
    'lvaquero',
    'visanche',
    'abermeom',
    'wguo',
    'xbie',
    'UNKNOWN',
    'bbasavas',
]

USER_COLORS: dict[str, str] = {
    username: '#' + ''.join(
        [
            str(hex(value // 3))[2:].zfill(2)
            for value in user_color[:3]
        ]
    )
    for username, user_color
    in zip(
        USERNAMES,
        cm.get_cmap('nipy_spectral')(
            np.linspace(0, 1, num=len(USERNAMES)),
            bytes=True
        )
    )
}

# keys: job_id
# value: dict containing 1 job information
JobDict = dict[str, dict[str, Any]]


def _run_remote_command_and_fetch_dict(update_cmd: list[str]) -> dict:

    update_cmd = ['ssh', 'access2-cp'] + update_cmd

    try:
        cmd_output: subprocess.CompletedProcess = subprocess.run(
            args=update_cmd,
            check=False,
            capture_output=True,
            text=True,
            timeout=TIMEOUT
        )

        return_code: int = cmd_output.returncode
        stdout: str = cmd_output.stdout
        stderr: str = cmd_output.stderr

        if return_code > 0:
            LOGGER.error(f"`{update_cmd[2]}` command failed with return code %i", return_code)
            print("\nstdout:\n", stdout)
            print("\nstderr:\n", stderr)

        elif return_code == 0:
            LOGGER.info(f"`{update_cmd[2]}` command was succesfull")

            return json.loads(stdout)

        raise subprocess.CalledProcessError(
            returncode=return_code,
            cmd=update_cmd
        )

    except subprocess.TimeoutExpired as timeout_exception:
        LOGGER.warning(f"`{update_cmd[2]}` command has timed out.")
        raise timeout_exception


def _fetch_and_parse_oarnodes_dict() -> tuple[dict, list[str]]:

    # oarnodes_dict has the individual CPU cores on the cluster as keys.
    # gpu1-7 each have 20 cores (2 CPUs with 10 cores each (no hyperthreading)).
    # gpu8 has 40 cores (2 CPUs with 20 cores each (no hyperthreading)).
    oarnodes_dict: dict = _run_remote_command_and_fetch_dict(
        update_cmd=['oarnodes', '-J', '--sql', '"cluster = \'perception\'"'])

    # Dictionnary mapping the nodes.
    nodes_dict: dict[str, dict] = {}

    # List of the job ids.
    jobs_list: list[str] = []

    # Looping through the cpu cores fetched from the oarnodes command.
    for cores, core_dict in oarnodes_dict.items():

        # Hostname
        host: str = core_dict['host']
        gpu_device: int = int(core_dict['gpudevice'])

        # If not alrady in the nodes_dict, add it
        if host not in nodes_dict:
            gpu_model_name: str = PRETTY_GPU_MODELS[core_dict['gputype']]
            nodes_dict[host] = {
                'gpu_model': f"{gpu_model_name} ({core_dict['gpumem']}GB)",
                'state': core_dict['state'],
                'gpus': {},
                'running_jobs': {}
            }

        # Get the dictionnary containing the (eventually incomplete) information about this node.
        node_dict: dict = nodes_dict[host]

        # If this cpu core is used
        if 'jobs' in core_dict:
            job_id: str = core_dict['jobs']
            if job_id in node_dict['running_jobs']:
                job: dict = node_dict['running_jobs'][job_id]
                job['nb_cores'] += 1
                if gpu_device not in job['gpu_ids']:
                    job['gpu_ids'].append(gpu_device)
            else:
                node_dict['running_jobs'][job_id] = {
                    'nb_cores': 1,
                    'gpu_ids': [gpu_device]
                }

                jobs_list.append(job_id)

        # This core is unused
        else:
            # Add the corresponding GPU to GPUs dict for this node
            if gpu_device not in node_dict['gpus']:
                node_dict['gpus'][gpu_device] = 'free'

    return nodes_dict, jobs_list


############################################
# OARSTAT (fetch running and waiting jobs) #
############################################

def _extract_walltime(message: str) -> str:
    match: Optional[re.Match] = re.compile(r"(?<=W=).+?(?=,)").search(message)

    walltime: str = ''
    if match:
        walltime = ':'.join(
            [
                sub.zfill(2)
                for sub in match.group().split(':')
            ]
        )

    return walltime


def _date_time_from_timestamp(timestamp: str) -> str:
    date_time: datetime = datetime.fromtimestamp(int(timestamp))

    return date_time.strftime(TIME_FORMAT)


def _get_max_time(job_dict: dict) -> str:
    start_time: datetime = datetime.fromtimestamp(job_dict['startTime'])

    walltime_str: str = _extract_walltime(job_dict['message'])

    max_time_str: str = ''
    if walltime_str:
        hours, mins, secs = walltime_str.split(':')
        walltime: timedelta = timedelta(
            hours=int(hours),
            minutes=int(mins),
            seconds=int(secs)
        )

        max_time: datetime = start_time + walltime
        max_time_str = max_time.strftime(TIME_FORMAT)

    return max_time_str


def _get_requested_gpu(properties: str) -> str:

    requested_gpu: str = ''
    match: Optional[re.Match] = re.compile(r"gpu[1-8]").search(properties)

    if match:
        requested_gpu = match.group()

    return requested_gpu


def _fetch_and_parse_oarstat_dict(
    jobs_running_on_perception_cluster: list[str]
) -> tuple[JobDict, JobDict]:

    oarstat_raw_dict: dict = _run_remote_command_and_fetch_dict(
        update_cmd=['oarstat', '-J']
    )

    running_jobs_dict: JobDict = {}
    waiting_jobs_dict: JobDict = {}

    for job_id, job_raw_dict in oarstat_raw_dict.items():
        # job_raw_dict: dict = oarstat_raw_dict[job_id]
        if 'perception' not in job_raw_dict['properties']:
            continue

        owner: str = job_raw_dict['owner']

        job_curated_dict: dict[str, Any] = {
            'id': int(job_raw_dict['Job_Id']),
            'name': job_raw_dict['name'],
            'owner': owner,
            'type': job_raw_dict['jobType'].lower(),
            'besteffort': 'besteffort' in job_raw_dict['types'],
            'idempotent': 'idempotent' in job_raw_dict['types'],
            'submission_time': _date_time_from_timestamp(job_raw_dict['submissionTime']),
            'start_time': _date_time_from_timestamp(job_raw_dict['startTime']),
            'walltime': _extract_walltime(job_raw_dict['message']),
            'max_time': _get_max_time(job_raw_dict),
            'requested_gpu': _get_requested_gpu(job_raw_dict['properties'])
        }
        state: str = job_raw_dict['state']

        if owner in USER_COLORS:
            job_curated_dict['owner_color'] = USER_COLORS[owner]
        else:
            job_curated_dict['owner_color'] = USER_COLORS['UNKNOWN']

        if state == 'Running' and job_id in jobs_running_on_perception_cluster:
            running_jobs_dict[job_id] = job_curated_dict
        elif state == 'Waiting':
            waiting_jobs_dict[job_id] = job_curated_dict

    return running_jobs_dict, waiting_jobs_dict


def _merge_running_jobs(
    nodes_dict: dict,
    jobs_dict: JobDict
) -> dict[int, dict]:

    output_dict: dict = {}

    for host, node_dict in nodes_dict.items():
        host_id: int = int(host[3])
        running_jobs: dict = node_dict.pop('running_jobs')
        for job_id, job_dict in running_jobs.items():
            for gpu_id in job_dict['gpu_ids']:
                job_dict.update(jobs_dict[job_id])
                node_dict['gpus'][gpu_id] = job_dict

        output_dict[host_id] = node_dict

    return output_dict


def update() -> dict:

    raw_nodes_dict: dict
    jobs_running_on_perception_cluster: list[str]
    raw_nodes_dict, jobs_running_on_perception_cluster = _fetch_and_parse_oarnodes_dict()

    running_jobs_dict: JobDict
    waiting_jobs_dict: JobDict
    (
        running_jobs_dict,
        waiting_jobs_dict
    ) = _fetch_and_parse_oarstat_dict(
        jobs_running_on_perception_cluster=jobs_running_on_perception_cluster
    )

    nodes_dict: dict = _merge_running_jobs(
        nodes_dict=raw_nodes_dict,
        jobs_dict=running_jobs_dict
    )
    cluster_dict: dict[str, dict] = {
        'nodes': nodes_dict,
        'waiting_jobs': waiting_jobs_dict
    }

    return cluster_dict

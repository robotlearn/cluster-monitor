#!/bin/sh

rsync -rav \
    --delete \
    --exclude "*.mypy_cache/" \
    --exclude "venv/" \
    --exclude ".git/" \
    --exclude "*/__pycache__/" \
    . alya:~/rl_hm_2/
    # . server:~/rl_hm_2/
